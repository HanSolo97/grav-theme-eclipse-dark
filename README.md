# Eclipse Dark Theme

The **Eclipse Dark** Theme is for [Grav CMS](http://github.com/getgrav/grav).

## Description

It is supposed to be as minimal as possible without lacking the most important functions.

## Screenshots

![EclipseDark Screenshot](screenshot.jpg)

## Demo

I didnt create a seperate Demo page, so my [Blog](https://blog.beckend.tk) has to be the Demo for now.

## Installation

1. Download the latest Zip from Releases and install it in the Grav Admin Interface under Tools -> Direct Install

2. Install the following Plugins:
- Archives
- Feed
- Pagination
- Random
- SimpleSearch
- TaxonomyList
- ReadingTime

3. Delete the Default Pages created by Grav, EXCEPT the Root Page! If you have existing Blogposts you can add them later as a child Element of the Blog Page.

4. Download the Template Pages from [Here]() and move it into the Grav user/pages/ Folder.

5. Adjust the Following Settings under Configuration -> System -> Content
- Set Home Page to Blog(/blog). Here you can Add your existing Blogposts as Child Elements of the Blog page.
- Tick Box at "Process TWIG"

## Configuration
- If you want to create a new Page/Post, it has to be a Child of the main /blog Page.
- Create a Imprint Page by creating a Page named "imprint" under the Root Element (/). Make sure you Click No for the Visibility, as the Theme doesnt have a Top Menu.
- Alternatively, you can also specify a custom Link for the Imprint in the Themes Settings.
- You can specify a custom Logo and Favicon in the Theme Settings.

## Future Plans:
- Make Color Changeable (Background and Text)

## Disclaimer
This is still in Beta Stadium.
Use it at your own Risk. This Software is provided as is with no warranty.

## Icons
The RSS Icons were obtained Free without attribution required. However, ill still link the Origin here:
- https://www.iconsdb.com/white-icons/rss-4-icon.html
- https://www.iconsdb.com/white-icons/rss-icon.html

## License
See the [License](LICENSE) File for more information.